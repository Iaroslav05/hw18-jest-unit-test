
import AuthRepository from "../services/auth.service";

describe("AuthRepository", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("should hash the password", () => {
    const password = "password123";
    const hashedPassword = AuthRepository.hash(password);
    expect(hashedPassword).toBeDefined();
  });

  it("should get user by email", async () => {
    const email = "test@example.com";

    (AuthRepository as any).db = {
      getRepository: jest.fn().mockReturnValue({
        createQueryBuilder: jest.fn().mockReturnThis(),
        where: jest.fn().mockReturnThis(),
        getOne: jest.fn().mockResolvedValue({ email }),
      }),
    };
    const user = await AuthRepository.getUserByEmail(email);
    expect(user.email).toBe(email);
  });

  it("should compare passwords correctly", async () => {
    const password = "password123";
    const hashedPassword = AuthRepository.hash(password);
    const result = await AuthRepository.comparePasswords(
      password,
      hashedPassword
    );
    expect(result).toBe(true);
  });

  it("should generate token correctly", async () => {
    const email = "test@example.com";
    const password = "password123";
    const token = await AuthRepository.generateToken(email, password);
    expect(token).toBeTruthy();
  });
});
