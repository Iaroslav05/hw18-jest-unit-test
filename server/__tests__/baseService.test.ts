import DatabaseService from "../services/database.service";
import Repository  from "../repository/Repository";
import { IParam } from "../interface/interface";

jest.mock("../repository/Repository.ts", () => ({
  count: jest.fn(),
  create: jest.fn(),
  readAll: jest.fn(),
  read: jest.fn(),
  update: jest.fn(),
  delete: jest.fn(),
}));

describe("DatabaseService", () => {
  afterEach(() => {
    jest.clearAllMocks();
  });

  it("should call Repository.count method", async () => {
    await DatabaseService.count("table");
    expect(Repository.count).toHaveBeenCalledWith("table");
  });

  it("should call Repository.create method", async () => {
    const data = { id: 1, name: "example" };
    await DatabaseService.create("table", data);
    expect(Repository.create).toHaveBeenCalledWith("table", data);
  });

  it("should call Repository.readAll method", async () => {
    const params: IParam = { page: 10, size: 0 };
    await DatabaseService.readAll("table", params);
    expect(Repository.readAll).toHaveBeenCalledWith("table", params);
  });

  it("should call Repository.read method", async () => {
    await DatabaseService.read("table", 1);
    expect(Repository.read).toHaveBeenCalledWith("table", 1);
  });

  it("should call Repository.update method", async () => {
    await DatabaseService.update("table", 1, "newData");
    expect(Repository.update).toHaveBeenCalledWith("table", 1, "newData");
  });

  it("should call Repository.delete method", async () => {
    await DatabaseService.delete("table", 1);
    expect(Repository.delete).toHaveBeenCalledWith("table", 1);
  });
});
